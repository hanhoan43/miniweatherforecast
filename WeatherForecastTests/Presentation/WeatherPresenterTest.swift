//
//  WeatherPresenterTest.swift
//  MiniWeatherForecastTests
//
//  Created by Bui Dat on 03/08/2022.
//  Copyright © 2022 Bui Dat. All rights reserved.
//

import XCTest
import Foundation
import UIKit
import Quick
import Nimble

@testable import MiniWeatherForecast

class WeatherPresenterTest: QuickSpec {
    override func spec() {
        let mockService = MockWeatherService(returnError: false, errorType: nil)
        let mockView = WeatherViewController()
        let sut = WeatherPresenter(view: mockView, weatherService: mockService)
        describe("TestWeatherForecastPresenter") {
            context("When Presenter is inited") {
                beforeEach {
                    sut.clearData()
                }
                it("And get the empty listCities", closure: {
                    expect(sut.numbersWeatherInSection)
                        .to(equal(0))
                    expect(sut.weather)
                        .to(beNil())
                })
            }
            
            context("When Presenter call API") {
                beforeEach {
                    sut.clearData()
                    mockService.reset()
                }
                
                it("Api key is invalid") {
                    mockService.returnError = true
                    sut.getWeather(term: "invalid", apiKey: "invalid")
                    expect(sut.numbersWeatherInSection)
                        .to(equal(0))
                }
                
                it("No data error") {
                    mockService.returnError = true
                    mockService.errorType = .noData
                    sut.getWeather(term: "invalid")
                    expect(sut.numbersWeatherInSection)
                        .to(equal(0))
                }
                
                it("Parse data error") {
                    mockService.returnError = true
                    mockService.errorType = .parseDataError
                    sut.getWeather(term: "invalid")
                    expect(sut.numbersWeatherInSection)
                        .to(equal(0))
                }
                
                it("Should return failed when out of range") {
                    let indexPath: IndexPath = IndexPath(item: -1, section: 0)
                    expect(sut.weatherForRowIndexPath(indexPath: indexPath))
                        .to(beNil())
                }
                
                it("Should get correct dt by index") {
                    let indexPath: IndexPath = IndexPath(item: 0, section: 0)
                    sut.getWeather(term: "Ho Chi Minh")
                    expect(sut.weatherForRowIndexPath(indexPath: indexPath)?.dt)
                        .to(equal(1659153600))
                }

            }
        }
    }
}
