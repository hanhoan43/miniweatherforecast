//
//  WeatherForecastTests.swift
//  WeatherForecastTests
//
//  Created by Bui Dat on 6/23/22.
//  Copyright © 2022 Bui Dat. All rights reserved.
//

import XCTest

@testable import MiniWeatherForecast

class WeatherForecastTests: XCTestCase {
    var sut: WeatherViewController!

    func makeSUT(returnError: Bool = false, errorType: ServiceError? = nil ) -> WeatherViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let sut = storyboard.instantiateViewController(identifier: "WeatherViewController") as! WeatherViewController
        sut.loadViewIfNeeded()
        
        let mockService = MockWeatherService(returnError: returnError, errorType: errorType)
        let mockPresenter = WeatherPresenter(view: sut, weatherService: mockService)
        sut.presenter = mockPresenter
        
        return sut
    }

    func testUserInput2Characters() {
        let sut = makeSUT()
        sut.searchBar.text = "Ho"
        sut.searchBarCancelButtonClicked(sut.searchBar)
        XCTAssertEqual(sut.presenter?.numbersWeatherInSection, 0)
    }

    func testUserInput2CharactersAndTapCancel() {
        let sut = makeSUT()
        sut.searchBar.text = "Ho"
        sut.searchBarSearchButtonClicked(sut.searchBar)
        XCTAssertEqual(sut.presenter?.numbersWeatherInSection, 0)
    }
    
    func testUserInputMoreThan3CharacterAndGetSuccessData() {
        let sut = makeSUT()
        sut.searchBar.text = "Ho Chi Minh"
        sut.searchBarSearchButtonClicked(sut.searchBar)
        XCTAssertEqual(sut.presenter?.numbersWeatherInSection, 2)
    }
    
    func testUserInputMoreThan3CharacterAndGetNoData() {
        let sut = makeSUT(returnError: true, errorType: ServiceError.noData)
        sut.searchBar.text = "Ho Chi Minh"
        sut.searchBarSearchButtonClicked(sut.searchBar)
        XCTAssertEqual(sut.presenter?.numbersWeatherInSection, 0)
    }

}
