//
//  WeatherServiceTest.swift
//  WeatherForecastTests
//
//  Created by Bui Dat on 29/07/2022.
//  Copyright © 2022 Bui Dat. All rights reserved.
//

import XCTest
@testable import MiniWeatherForecast

class WeatherServiceTest: XCTestCase {

    func testWeatherServiceFetchFail() throws {
        let mockApiService = MockApiService(returnError: true, errorType: nil)
        let weatherService = WeatherService(apiClient: mockApiService)
        weatherService.getWeatherDailyWeather(fromParams: nil) { response in
            switch response {
            case .success(_): break
            case .failure(let error):
                XCTAssertNotNil(error)
            }
        }
    }
    
    func testWeatherServiceFetchSuccess() throws {
        let mockApiService = MockApiService(returnError: false, errorType: nil)
        let weatherService = WeatherService(apiClient: mockApiService)
        weatherService.getWeatherDailyWeather(fromParams: nil) { response in
            switch response {
            case .success(let response):
                XCTAssertNotNil(response)
            case .failure(_): break
            }
        }
    }

    func testWeatherServiceSuccessButCantParseData() throws {
        let mockApiService = MockApiService(returnError: false, errorType: .parseDataError)
        let weatherService = WeatherService(apiClient: mockApiService)
        weatherService.getWeatherDailyWeather(fromParams: nil) { response in
            switch response {
            case .success(_): break
            case .failure(let error):
                XCTAssertNotNil(error)
            }
        }
    }
    
    func testApiServiceURL() throws {
        let params = [
            "q": "saigon",
            "cnt": "7",
            "appid": Bundle.main.getInfoByKey(key: NetworkConstants.openWeatherAPIKey) as! String,
            "units": "metric",
        ]
        let mocApiService = MockApiService(returnError: false, errorType: nil)
        let url = mocApiService.getURL(scheme: NetworkConstants.secureConnection,
                                       host: NetworkConstants.openWeatherDomain,
                                       path: NetworkConstants.openWeatherDaily,
                                       params: params
        )
        XCTAssertEqual(url?.absoluteString, "https://api.openweathermap.org/data/2.5/forecast/daily?appid=60c6fbeb4b93ac653c492ba806fc346d&cnt=7&q=saigon&units=metric")
    }

}
