//
//  Sanitizer.swift
//  WeatherForecastTests
//
//  Created by Bui Dat on 29/07/2022.
//  Copyright © 2022 Bui Dat. All rights reserved.
//

import XCTest
@testable import MiniWeatherForecast

class SanitizerTest: XCTestCase {

    func testStringSanitizer() throws {
        let sanitizedString = Sanitier.stringSanitizer("SAI GON")
        XCTAssertEqual(sanitizedString, "saigon")
    }

}
