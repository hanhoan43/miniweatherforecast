//
//  Formatter.swift
//  WeatherForecastTests
//
//  Created by Bui Dat on 29/07/2022.
//  Copyright © 2022 Bui Dat. All rights reserved.
//

import XCTest
@testable import MiniWeatherForecast

class FormatterTest: XCTestCase {

    func testDateFormat() throws {
        let date = Formatter.timeStampToDate(1659102229)
        XCTAssertEqual(date, "Fri, 29 July 2022")
    }

}
