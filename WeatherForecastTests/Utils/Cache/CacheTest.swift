//
//  Cache.swift
//  WeatherForecastTests
//
//  Created by Bui Dat on 29/07/2022.
//  Copyright © 2022 Bui Dat. All rights reserved.
//

import XCTest
@testable import MiniWeatherForecast

class CacheTest: XCTestCase {
    override func setUpWithError() throws {
        DataCache.defaultInstance.removeAll()
    }

    override func tearDownWithError() throws {
        DataCache.defaultInstance.removeAll()
    }

    func testEmptyCacheData() {
        let sut = DataCache.defaultInstance
        XCTAssertEqual(sut.readString(forKey: "empty"), nil)
    }
    
    func testDataOnDiskNotMemory() {
        let sut = DataCache.defaultInstance
        let queue = DispatchQueue(label: "test_read_disk_data")
        let expectation = expectation(description: "In queue")
        queue.async {
            sut.write(string: "This is cache data", forKey: "test disk cache") { result in
                if (result) {
                    sut.removeMemCache()
                    expectation.fulfill()
                } else {
                    XCTAssertFalse(result)
                }
            }
        }
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertEqual(sut.readString(forKey: "test disk cache"), "This is cache data", "They are not equal")
    }
    
    func testDataInMemoryNotDisk() {
        let sut = DataCache.defaultInstance
        let queue = DispatchQueue(label: "test_read_memmory_data")
        let expectation = self.expectation(description: "In queue")
        queue.async {
            sut.write(string: "This is cache data", forKey: "test disk cache") { result in
                if (result) {
                    sut.removeDiskCache{ result in
                        if (result) {
                            expectation.fulfill()
                        } else {
                            XCTAssertFalse(result)
                        }
                    }
                    expectation.fulfill()
                } else {
                    XCTAssertFalse(result)
                }
            }
        }
        
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertEqual(sut.readString(forKey: "test disk cache"), "This is cache data", "They are not equal")
    }
    
    func testWriteDataToCacheAndDisk() {
        let sut = DataCache.defaultInstance
        let queue = DispatchQueue(label: "test_write_data")
        let expectation = self.expectation(description: "In queue")
        queue.async {
            sut.write(string: "This is cache data", forKey: "test disk cache") { result in
                if (result) {
                    expectation.fulfill()
                } else {
                    XCTAssertFalse(result)
                }
            }
        }

        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertEqual(sut.readString(forKey: "test disk cache"), "This is cache data", "They are not equal")
    }
    
    func testCleanDataCache() {
        let sut = DataCache.defaultInstance
        let queue = DispatchQueue(label: "test_clean_data")
        let expectation = self.expectation(description: "In queue")
        queue.async {
            sut.write(string: "This is cache data", forKey: "test clean") { result in
                if (result) {
                    if (result) {
                        sut.remove(byKey: "test clean")
                        expectation.fulfill()
                    } else {
                        XCTAssertFalse(result)
                    }
                } else {
                    XCTAssertFalse(result)
                }
            }
        }

        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertEqual(sut.readString(forKey: "test clean"), nil)
        XCTAssertNil(sut.memCache.object(forKey: "test clean" as AnyObject))
    }

}
