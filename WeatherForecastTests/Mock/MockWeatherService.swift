//
//  MockWeatherService.swift
//  MiniWeatherForecastTests
//
//  Created by Bui Dat on 03/08/2022.
//  Copyright © 2022 Bui Dat. All rights reserved.
//

import Foundation

@testable import MiniWeatherForecast

final class MockWeatherService {
    var returnError = false
    var errorType: ServiceError?
    
    init(returnError: Bool, errorType: ServiceError?) {
        self.returnError = returnError
        self.errorType = errorType
    }
    
    func reset() {
        returnError = false
        errorType = nil
    }
}

extension MockWeatherService: WeatherServiceProtocol {
    func getWeatherDailyWeather(fromParams params: [String:String]?, completion: @escaping (Result<WeatherForecast, ServiceError>) -> Void) {
        if (returnError) {
            completion(.failure(errorType ?? .unknown))
        } else {
            completion(.success(MockWeatherForecastDataResponse.weatherForecastData))
        }
    }
}

