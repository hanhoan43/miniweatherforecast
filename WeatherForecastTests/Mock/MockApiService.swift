//
//  MockApiService.swift
//  WeatherForecastTests
//
//  Created by Bui Dat on 30/07/2022.
//  Copyright © 2022 Bui Dat. All rights reserved.
//

import Foundation

@testable import MiniWeatherForecast

final class MockApiService: ApiServiceProtocol {
    var returnError = false
    var errorType: ServiceError?
    
    init(returnError: Bool, errorType: ServiceError?) {
        self.returnError = returnError
        self.errorType = errorType
    }
    
    func getAPI<T>(url: URL, timeout: TimeInterval, isUsingCached: Bool, completion: @escaping (Result<ResponseJSONDecoder<T>, ServiceError>) -> Void) where T : Decodable, T : Encodable {
        if (returnError) {
            completion(.failure(errorType ?? .unknown))
        } else {
            let encoder = JSONEncoder()
            let mock = MockWeatherForecastDataResponse.weatherForecastData
            do {
                let mockData = try encoder.encode(mock)
                if (errorType != nil) {
                    let response = try ResponseJSONDecoder<T>(data: nil)
                    completion(.success(response))
                } else {
                    let response = try ResponseJSONDecoder<T>(data: mockData)
                    completion(.success(response))
                }
            } catch {
                completion(.failure(errorType ?? .parseDataError))
            }
        }
        
    }
    
    func getURL(scheme: String, host: String, path: String, params: [String : String]) -> URL? {
        var components = URLComponents()
        components.scheme = scheme
        components.host = host
        components.path = path
        components.queryItems = []
        for name in params.keys.sorted() {
            if let value = params[name] {
                let item = URLQueryItem(
                    name: "\(name)",
                    value: "\(value)"
                )
                components.queryItems?.append(item)
            }
        }
        return components.url
    }
    
}
