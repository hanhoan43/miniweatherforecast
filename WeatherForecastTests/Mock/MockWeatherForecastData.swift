//
//  MockWeatherForecastData.swift
//  WeatherForecastTests
//
//  Created by Bui Dat on 30/07/2022.
//  Copyright © 2022 Bui Dat. All rights reserved.
//

import Foundation
@testable import MiniWeatherForecast

struct MockWeatherForecastDataResponse {
    // City data
    private static let city = City(id: 1580578, name: "Ho Chi Minh City", coord: nil, country: nil, population: nil, timezone: nil)
    
    // Fisrt weather data detail
    private static let firstWeatherDetail = WeatherDetail(dt: 1659153600, sunrise: nil, sunset: nil, temp: firstTemp, fellLike: nil, pressure: 1007,
                                                          humidity: 61, weather: firstWeather, speed: nil, deg: nil, gust: nil, clouds: nil, pop: nil,
                                                          rain: nil)
    private static let firstWeather = [
        Weather(id: 1, main: "Rain", description: "moderate rain", icon: nil)
    ]
    private static let firstTemp = DayTemp(day: 29, min: 27, max: 32, night: 29, eve: 30, morn: 29)
    
    // Second weather data detail
    private static let secondWeatherDetail = WeatherDetail(dt: 1659240000, sunrise: nil, sunset: nil, temp: secondTemp, fellLike: nil, pressure: 1006,
                                                           humidity: 74, weather: secondWeather, speed: nil, deg: nil, gust: nil, clouds: nil, pop: nil,
                                                           rain: nil)
    private static let secondWeather = [
        Weather(id: 1, main: "sunny", description: "sunny", icon: nil)
    ]
    private static let secondTemp = DayTemp(day: 27, min: 25, max: 29, night: 26, eve: 24, morn: 26)
    
    // Mock data
    static let weatherForecastData = WeatherForecast(city: city, cod: nil, message: nil, cnt: 7, list: [firstWeatherDetail, secondWeatherDetail])
}


