# Weather Forecast

Weather Forecast Swift iOS app showcasing basic ParticleSetup / Particle-SDK cocoapods usage / getting started.

Using openweathermap.org to get weather data

Built using XCode 12.4 (Swift 4)

### How to run the example?

1. Clone this repo
1. Open `WeatherForecast.xcworkspace` and run the project on selected device or simulator

# Architecture and design patterns

## MVP

- The models: that are a representation of your data.
- The view: consists of both views and view controllers, with all UI setup and events.
- The presenter will be in charge of all the logic , including responding to user actions and updating the UI (via delegate).

# Folder structure

- **Constants** folder contains files that store variables using in the application
- **Extension** folder contains the extensions that help function easy to use
- **Resource** folder contains configuration of the project
- **Utils** folder contains utility classes that are used throughout the project
- **Services** folder contains unit of logic that runs in the network like API call
- **Model** folder contains classes or structures that hold data
- **Presenter** folder contains Presneter classes of each screen
- **View** folder contains View classes and XIB file of each screen

# Third parties frameworks

- **Quick** - is a behavior-driven development framework for Swift and Objective-C

- **Nimble** - express the expected outcomes of Swift or Objective-C expressions

# Items that has been DONE

- [x] The application is a simple iOS application which is written by Swift.
- [x] The application is able to retrieve the weather information from OpenWeatherMaps API.
- [x] The application is able to allow user to input the searching term.
- [x] The application is able to proceed searching with a condition of the search term length must be from 3 characters or above.
- [x] The application is able to render the searched results as a list of weather items.
- [x] The application is able to support caching mechanism so as to prevent the app from generating a bunch of API requests.
- [x] The application is able to manage caching mechanism & lifecycle.
- [x] The application is able to handle failures.
- [x] The application is able to support the disability to scale large text for who can't see the text clearly.
- [x] The application is able to support the disability to read out the text using VoiceOver controls.
