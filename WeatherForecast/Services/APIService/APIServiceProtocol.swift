//
//  APIServiceProtocol.swift
//  WeatherForecast
//
//  Created by Bui Dat on 7/24/22.
//  Copyright © 2022 Bui Dat. All rights reserved.
//

import Foundation

protocol ApiServiceProtocol {
    func getAPI<T: Codable>(url: URL, timeout: TimeInterval, isUsingCached: Bool, completion: @escaping (Result<ResponseJSONDecoder<T>, ServiceError>) -> Void)
    func getURL(scheme: String, host: String, path: String, params: [String: String]) -> URL?
}
