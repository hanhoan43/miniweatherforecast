//
//  APIService.swift
//  WeatherForecast
//
//  Created by Bui Dat on 7/22/22.
//  Copyright © 2022 Bui Dat. All rights reserved.
//

import Foundation

struct ResponseJSONDecoder<T: Decodable> {
    let entity: T
    let data: Data?
    
    init(data: Data?) throws {
        do {
            let decoder = JSONDecoder()
            self.entity = try decoder.decode(T.self, from: data ?? Data())
            self.data = data
        } catch {
            throw ServiceError.parseDataError
        }
    }
}


public enum ServiceError: Error {
    case apiError
    case invalidResponse
    case noData
    case parseDataError
    case unknown
}

class APIService {
    private func getDataFromURL<T: Codable>(
        url: URL,
        urlSessionConfiguration: URLSessionConfiguration,
        isUsingCached: Bool,
        completionHandler: @escaping (Result<ResponseJSONDecoder<T>, ServiceError>) -> Void) {
            if let cachedResponseString = DataCache.defaultInstance.readString(forKey: url.absoluteString) {
                let cachedData = cachedResponseString.data(using: String.Encoding.utf8)
                 do {
                     let responseData = try ResponseJSONDecoder<T>(data: cachedData)
                     completionHandler(.success(responseData))
                 } catch (let error) {
                     print(error.localizedDescription)
                     completionHandler(.failure(ServiceError.parseDataError))
                 }
            } else {
                let task = URLSession(configuration: urlSessionConfiguration).dataTask(with: url) { data, response, error in
                    guard let data = data else { return completionHandler(.failure(ServiceError.noData)) }
                    if error != nil {
                        // OH NO! An error occurred...
                        return completionHandler(.failure(error as! ServiceError))
                    }
                    guard let httpResponse = response as? HTTPURLResponse,
                          (200...299).contains(httpResponse.statusCode) else {
                        return completionHandler(.failure(ServiceError.apiError))
                    }
                    
                    do {
                        guard let json = String(data: data, encoding: String.Encoding.utf8) else {
                            completionHandler(.failure(ServiceError.parseDataError))
                            return
                        }
                        if (isUsingCached) {
                            DataCache.defaultInstance.write(string: json, forKey: url.absoluteString) { result in
                                if (result) {
                                    print("Write to disk success")
                                } else {
                                    print("Write to disk fail")
                                }
                            }
                        }
                        
                        let responseData = try ResponseJSONDecoder<T>(data: data)
                        return completionHandler(.success(responseData))
                    } catch DecodingError.dataCorrupted {
                        return completionHandler(.failure(ServiceError.parseDataError))
                    } catch {
                        return completionHandler(.failure(ServiceError.unknown))
                    }
                }
                task.resume()
            }
        }
}

extension APIService {
    public func getURL(scheme: String, host: String, path: String, params: [String: String]) -> URL? {
        var components = URLComponents()
        components.scheme = scheme
        components.host = host
        components.path = path
        components.queryItems = []
        for name in params.keys.sorted() {
            if let value = params[name] {
                let item = URLQueryItem(
                    name: "\(name)",
                    value: "\(value)"
                )
                components.queryItems?.append(item)
            }
        }
        return components.url
    }
}

extension APIService: ApiServiceProtocol {
    func getAPI<T: Codable>(url: URL, timeout: TimeInterval, isUsingCached: Bool, completion: @escaping (Result<ResponseJSONDecoder<T>, ServiceError>) -> Void) {
        let sessionConfig = URLSessionConfiguration.default
        sessionConfig.timeoutIntervalForRequest = 30.0
        sessionConfig.timeoutIntervalForResource = 60.0
        getDataFromURL(url: url, urlSessionConfiguration: sessionConfig, isUsingCached: isUsingCached , completionHandler: completion)
    }
}
