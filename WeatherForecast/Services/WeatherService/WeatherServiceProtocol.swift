//
//  WeatherServiceProtocol.swift
//  WeatherForecast
//
//  Created by Bui Dat on 7/23/22.
//  Copyright © 2022 Bui Dat. All rights reserved.
//

import Foundation

protocol WeatherServiceProtocol {
  func getWeatherDailyWeather(fromParams params: [String:String]?, completion: @escaping (Result<WeatherForecast, ServiceError>) -> Void)
}
