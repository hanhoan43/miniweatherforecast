//
//  WeatherService.swift
//  WeatherForecast
//
//  Created by Bui Dat on 7/23/22.
//  Copyright © 2022 Bui Dat. All rights reserved.
//

import Foundation

final class WeatherService: WeatherServiceProtocol {
    
    var apiClient: ApiServiceProtocol?
    
    init(apiClient: ApiServiceProtocol) {
        self.apiClient = apiClient
    }
    
    func getWeatherDailyWeather(fromParams params: [String : String]?, completion: @escaping (Result<WeatherForecast, ServiceError>) -> Void) {
        let parameters = params ?? ["q":"saigon"]
        guard let url = self.apiClient?.getURL(scheme: NetworkConstants.secureConnection, host: NetworkConstants.openWeatherDomain, path: NetworkConstants.openWeatherDaily, params: parameters) else { return }
        
        self.apiClient?.getAPI(url: url, timeout: 30, isUsingCached: true, completion: { (result: Result<ResponseJSONDecoder<WeatherForecast>, ServiceError>) in
            switch result {
            case let .success(response):
                completion(.success(response.entity))
            case let .failure(error):
                completion(.failure(error))
            }
        })
    }
}
