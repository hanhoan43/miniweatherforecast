//
//  Cache.swift
//  WeatherForecast
//
//  Created by Bui Dat on 7/4/22.
//  Copyright © 2022 Bui Dat. All rights reserved.
//

import Foundation

enum CacheError: Error {
    case invalidSelection
    case outOfDate
    case dataInvalid
}

// Data wraper
final class Entry: NSObject, NSCoding, NSSecureCoding {
    static var supportsSecureCoding: Bool {
        return true
    }
    
    let expirationDate: Date
    
    let data: AnyObject
    
    public init(data: AnyObject, expirationDate: Date) {
        self.data = data
        self.expirationDate = expirationDate
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(data, forKey: "data")
        coder.encode(expirationDate, forKey: "expirationDate")
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.data = aDecoder.decodeObject(forKey: "data")! as AnyObject
        self.expirationDate = aDecoder.decodeObject(forKey:"expirationDate") as! Date
    }
    
}

// Data cache manager
open class DataCache {
    // Default settings
    static let cacheDirectoryPrefix = "com.scorpio.cache."
    static let ioQueuePrefix = "com.scorpio.queue."
    static let defaultEntryLifeTime: TimeInterval = 5 * 60
    static let defaultMaximumEntryCount = 50
    
    // Default cache instance
    public static let defaultInstance = DataCache(name: "default")
    
    let memCache = NSCache<AnyObject, AnyObject>()
    let ioQueue: DispatchQueue
    let fileManager: FileManager
    let cachePath: String
    
    open var name: String = ""
    
    open var entryLifetime: TimeInterval
    
    
    init(name: String,
         path: String? = nil,
         entryLifetime: TimeInterval? = nil,
         maximumEntryCount: Int? = nil) {
        self.name = name
        self.entryLifetime = entryLifetime ?? DataCache.defaultEntryLifeTime
        memCache.countLimit = maximumEntryCount ?? DataCache.defaultMaximumEntryCount
        
        var cachePath = path ?? NSSearchPathForDirectoriesInDomains(.cachesDirectory, FileManager.SearchPathDomainMask.userDomainMask, true).first!
        cachePath = (cachePath as NSString).appendingPathComponent(DataCache.cacheDirectoryPrefix + name)
        self.cachePath = cachePath
        
        ioQueue = DispatchQueue(label: DataCache.ioQueuePrefix + name)
        
        self.fileManager = FileManager()
    }
}

// Write data section
extension DataCache {
    
    // Write data by key
    public func write(data: Data, forKey key: String, completion: @escaping (Bool) -> Void) {
        let entry = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data) as? Entry
        memCache.setObject(entry as AnyObject, forKey: key as AnyObject)
        writeDataToFile(data: data, key: key, completion: completion)
    }
    
    private func writeDataToFile(data: Data, key: String, completion: @escaping (Bool) -> Void) {
        ioQueue.async {
            if self.fileManager.fileExists(atPath: self.cachePath) == false {
                do {
                    try self.fileManager.createDirectory(atPath: self.cachePath, withIntermediateDirectories: true, attributes: nil)
                } catch {
                    print("An error has occurred while creating cache folder: \(error.localizedDescription)")
                }
            }
            
            let bullshit = self.fileManager.createFile(atPath: self.cachePath(forKey: key), contents: data, attributes: nil)
            completion(bullshit)
        }
    }
    
    // Read data by key
    public func readData(forKey key: String) -> Data? {
        if let entry = memCache.object(forKey: key as AnyObject) as? Entry {
            guard Date() < entry.expirationDate else {
                remove(byKey: key)
                return nil
            }
            do {
                let data = try NSKeyedArchiver.archivedData(withRootObject: entry.data, requiringSecureCoding: true)
                return data
            } catch {
                return nil
            }
        } else {
            //if no data on memory then it will load data from disk
            if let dataFromDisk = readDataFromFile(forKey: key),
               let entry = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(dataFromDisk) as? Entry {
                guard Date() < entry.expirationDate else {
                    remove(byKey: key)
                    return nil
                }
                memCache.setObject(entry as AnyObject, forKey: key as AnyObject)
                do {
                    let data = try NSKeyedArchiver.archivedData(withRootObject: entry.data, requiringSecureCoding: true)
                    return data
                } catch {
                    return nil
                }
            } else {
                return nil
            }
        }
    }
    
    // Read data from file by key
    public func readDataFromFile(forKey key: String) -> Data? {
        return self.fileManager.contents(atPath: cachePath(forKey: key))
    }
    
}

// Write object section
extension DataCache {
    // Write object by key. This object must inherit from `NSObject` and implement `NSCoding` protocol. `String`, `Array`, `Dictionary` conform to this method.
    public func write(object: NSCoding, forKey key: String, completion: @escaping (Bool) -> Void) {
        do {
            let date = Date().addingTimeInterval(self.entryLifetime)
            let entry = Entry(data: object, expirationDate: date)
            let entryData = try NSKeyedArchiver.archivedData(withRootObject: entry, requiringSecureCoding: true)
            write(data: entryData, forKey: key, completion: completion)
        } catch let error {
            print(error)
            return
        }
    }
    
    // Write a string for key
    public func write(string: String, forKey key: String, completion: @escaping (Bool) -> Void) {
        write(object: string as NSCoding, forKey: key, completion: completion)
    }
    
    // Read an object by key. This object must inherit from `NSObject` and implement NSCoding protocol. `String`, `Array`, `Dictionary` conform to this method
    public func readObject(forKey key: String) -> NSObject? {
        let data = readData(forKey: key)
        
        if let data = data {
            do {
                let unarchivedData = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data) as? NSObject
                return unarchivedData
            } catch {
                return nil
            }
        }
        
        return nil
    }
    
    // Read a encoded string for key
    public func readString(forKey key: String) -> String? {
        return readObject(forKey: key) as? String
    }
    
}

// Remove cache section
extension DataCache {
    public func removeAll() {
        removeMemCache()
        removeDiskCache() { result in
            print(result)
        }
    }
    
    // Remove cache by key
    public func remove(byKey key: String) {
        memCache.removeObject(forKey: key as AnyObject)
        
        ioQueue.async {
            do {
                try self.fileManager.removeItem(atPath: self.cachePath(forKey: key))
            } catch {
                print("An error has occurred when remove file: \(error.localizedDescription)")
            }
        }
    }
    
    // Remove cache from memory
    public func removeMemCache() {
        memCache.removeAllObjects()
    }
    
    // Remove cache from disk
    public func removeDiskCache(completion: @escaping (Bool)->Void) {
        ioQueue.async {
            do {
                let result = try self.fileManager.removeItem(atPath: self.cachePath) as? Bool
                if let result = result {
                    completion(result)
                }
            } catch {
                print("An error has occurred when clean disk: \(error.localizedDescription)")
            }
        }
    }
    
}

extension DataCache {
    func cachePath(forKey key: String) -> String {
        let fileName = Data(key.utf8).base64EncodedString()
        return (cachePath as NSString).appendingPathComponent(fileName)
    }
}
