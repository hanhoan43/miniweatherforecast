//
//  Formater.swift
//  WeatherForecast
//
//  Created by Bui Dat on 27/07/2022.
//  Copyright © 2022 Bui Dat. All rights reserved.
//

import Foundation

class Formatter {
    public static func timeStampToDate(_ timeStamp: Double) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
        dateFormatter.dateFormat = "EEE, dd MMMM yyyy"

        let date = Date(timeIntervalSince1970: timeStamp)
        let strDate = dateFormatter.string(from: date)
        return strDate
    }
}
