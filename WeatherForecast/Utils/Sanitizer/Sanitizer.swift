//
//  Sanitizer.swift
//  WeatherForecast
//
//  Created by Bui Dat on 7/22/22.
//  Copyright © 2022 Bui Dat. All rights reserved.
//

import Foundation

class Sanitier {
  public static func stringSanitizer(_ string: String) -> String {
    return string.filter{!$0.isWhitespace}.lowercased()
  }
}
