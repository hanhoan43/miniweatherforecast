//
//  BundleExt.swift
//  MiniWeatherForecast
//
//  Created by Bui Dat on 04/08/2022.
//  Copyright © 2022 Bui Dat. All rights reserved.
//

import Foundation

extension Bundle {
    func getInfoByKey(key: String) -> Any? {
        guard let result = Bundle.main.infoDictionary?[key] else {
            return nil
        }
        return result
    }
}
