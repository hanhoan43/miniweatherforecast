//
//  Weather.swift
//  WeatherForecast
//
//  Created by Bui Dat on 6/24/22.
//  Copyright © 2022 Bui Dat. All rights reserved.
//

import Foundation

struct WeatherForecast: Codable {
    let city: City?
    let cod: String?
    let message: Float?
    let cnt: Int?
    let list: [WeatherDetail]?
}

struct City: Decodable, Encodable {
    let id: Int
    let name: String?
    let coord: Coord?
    let country: String?
    let population: Int?
    let timezone: Int?
}

struct Coord: Decodable, Encodable {
    let lon: Float?
    let lat: Float?
}

struct WeatherDetail: Decodable, Encodable {
    let dt: Double?
    let sunrise: Int?
    let sunset: Int?
    let temp: DayTemp?
    let fellLike: FeelsLike?
    let pressure: Int?
    let humidity: Int?
    let weather: [Weather]?
    let speed: Float?
    let deg: Int?
    let gust: Float?
    let clouds: Int?
    let pop: Float?
    let rain: Float?
}

struct DayTemp: Decodable, Encodable {
    let day: Float?
    let min: Float?
    let max: Float?
    let night: Float?
    let eve: Float?
    let morn: Float?
}

struct FeelsLike: Decodable, Encodable {
    let day: Float?
    let night: Float?
    let eve: Float?
    let morn: Float?
}

struct Weather: Decodable, Encodable {
    let id: Int
    let main: String?
    let description: String?
    let icon: String?
}
