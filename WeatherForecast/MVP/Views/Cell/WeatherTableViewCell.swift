//
//  WeatherTableViewCell.swift
//  WeatherForecast
//
//  Created by Bui Dat on 6/24/22.
//  Copyright © 2022 Bui Dat. All rights reserved.
//

import UIKit

class WeatherTableViewCell: UITableViewCell {
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblAverageTemperature: UILabel!
    @IBOutlet weak var lblPressure: UILabel!
    @IBOutlet weak var lblHumidity: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblDate.isAccessibilityElement = true
        lblDate.accessibilityHint = "The date of the weather forecast"
        
        lblAverageTemperature.isAccessibilityElement = true
        lblAverageTemperature.accessibilityHint = "The average temperature of the weather forecast"
        
        lblPressure.isAccessibilityElement = true
        lblPressure.accessibilityHint = "The pressure of the weather forecast"
        
        lblHumidity.isAccessibilityElement = true
        lblHumidity.accessibilityHint = "The humidity of the weather forecast"
        
        lblDescription.isAccessibilityElement = true
        lblDescription.accessibilityHint = "The description of the weather forecast"
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configureCell(with weatherDetail: WeatherDetail) {
        if let dt = weatherDetail.dt {
            let dateString = Formatter.timeStampToDate(dt)
            lblDate.text = "Date: " + dateString
            lblDate.accessibilityValue = dateString
        } else {
            lblDate.text = "Nothing to show"
        }
        if let temp = weatherDetail.temp?.day {
            let averageTempText = String(format: "%.1f°C", temp)
            lblAverageTemperature.text = "Avg Temp: " + averageTempText
            lblAverageTemperature.accessibilityValue = "\(temp) degrees Celsius"
        } else {
            lblAverageTemperature.text = "Nothing to show"
        }
        if let pressure = weatherDetail.pressure {
            lblPressure.text = "Pressure: " + String(pressure)
            lblPressure.accessibilityValue = "\(pressure) hPa"
        } else {
            lblPressure.text = "Nothing to show"
        }
        if let humidity = weatherDetail.humidity {
            lblHumidity.text = "Humidity: " + "\(String(humidity)) %"
            lblHumidity.accessibilityValue = "\(humidity) percent"
        } else {
            lblHumidity.text = "Nothing to show"
        }
        if let description = weatherDetail.weather?[0].description {
            lblDescription.text = "Description: " + description
            lblDescription.accessibilityValue = description
        } else {
            lblDescription.text = "Nothing to show"
        }
    }
}
