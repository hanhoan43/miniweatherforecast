//
//  WeatherViewController.swift
//  WeatherForecast
//
//  Created by Bui Dat on 6/23/22.
//  Copyright © 2022 Bui Dat. All rights reserved.
//

import UIKit

class WeatherViewController: UIViewController {
    
    @IBOutlet weak var errorView: ErrorView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var myTableView: UITableView!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    
    var presenter: WeatherPresenter!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupNavigationBar()
        setupSearchBar()
        setupTableView()
        setupPresenter()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
}

extension WeatherViewController {
    func setupNavigationBar() {
        title = "Weather Forecast"
    }
}

// Loading indicator setup
extension WeatherViewController: WeatherForecastView {
    
    func showLoading() {
        DispatchQueue.main.async { [weak self] in
            self?.activityIndicatorView.startAnimating()
        }
    }
    
    func hideLoading() {
        DispatchQueue.main.async { [weak self] in
            self?.activityIndicatorView.stopAnimating()
        }
    }
}

extension WeatherViewController {
    func setupSearchBar() {
        searchBar.delegate = self
        searchBar.showsCancelButton = true
    }
}

extension WeatherViewController {
    private func setupTableView() {
        // Dynamic height
        myTableView.estimatedRowHeight = 200
        myTableView.rowHeight = UITableView.automaticDimension
        
        // Disable keyboard
        myTableView.keyboardDismissMode = .onDrag
        
        // Table register
        myTableView.delegate = self
        myTableView.dataSource = self
        myTableView.register(UINib(nibName: "WeatherTableViewCell", bundle: nil), forCellReuseIdentifier: "WeatherTableViewCell")
    }
}

extension WeatherViewController {
    func setupPresenter() {
        presenter = WeatherPresenter(view: self, weatherService: WeatherService(apiClient: APIService()))
        presenter.getWeather()
    }
    
    // Presenter delegate
    func presentWeather() {
        DispatchQueue.main.async {
            self.myTableView.reloadData()
            self.myTableView.isHidden = false
            self.errorView.hideError()
        }
    }
    
    func presentAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            switch action.style{
                case .default:
                print("default")

                case .cancel:
                print("cancel")

                case .destructive:
                print("destructive")

            @unknown default:
                print("default")
            }
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func presentError(title: String, message: String) {
        errorView.presentError(title: title, message: message)
        myTableView.isHidden = true
    }
}

// UISearchBar delegate
extension WeatherViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let term = searchBar.text, term.count > 2 else {
            presenter?.showInvalidInput()
            return
        }
        searchBar.endEditing(true)
        presenter.getWeather(term: term)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
}

// UITableView delegate
extension WeatherViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter?.numbersWeatherInSection ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WeatherTableViewCell", for: indexPath) as! WeatherTableViewCell
        guard let weather = presenter?.weatherForRowIndexPath(indexPath: indexPath) else { return UITableViewCell() }
        cell.configureCell(with: weather)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true )
    }
}
