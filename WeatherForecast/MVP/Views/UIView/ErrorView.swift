//
//  ErrorView.swift
//  MiniWeatherForecast
//
//  Created by Scorpio on 8/13/22.
//  Copyright © 2022 Bui Dat. All rights reserved.
//

import UIKit

class ErrorView: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
    }
    
    func setupView() {
        Bundle.main.loadNibNamed("ErrorView", owner: self, options: nil)
        self.addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight,.flexibleWidth]
    }
    
    func presentError(title: String, message: String) {
        lblTitle.text = title
        lblDescription.text = message
        self.isHidden = false
    }
    
    func hideError() {
        self.isHidden = true
    }
    
}
