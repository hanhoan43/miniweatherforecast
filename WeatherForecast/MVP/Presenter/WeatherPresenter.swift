//
//  WeatherPresenter.swift
//  WeatherForecast
//
//  Created by Bui Dat on 6/24/22.
//  Copyright © 2022 Bui Dat. All rights reserved.
//

import Foundation
import UIKit

protocol WeatherForecastView: AnyObject {
    func presentWeather()
    func showLoading()
    func hideLoading()
    func presentAlert(title: String, message: String)
    func presentError(title: String, message: String)
}

protocol WeatherPresenterDelegate: AnyObject {
    var numbersWeatherInSection: Int { get }

    func getWeather(term: String)
    func weatherForRowIndexPath(indexPath: IndexPath) -> WeatherDetail?
    func showInvalidInput()
}

typealias PresenterDelegate = WeatherPresenterDelegate & UIViewController

class WeatherPresenter {
    let weatherService: WeatherServiceProtocol
    weak var view: WeatherForecastView?
    
    init(view: WeatherForecastView, weatherService: WeatherServiceProtocol) {
        self.view = view
        self.weatherService = weatherService
    }
    
    public private(set) var weather: WeatherForecast?
}

extension WeatherPresenter {
    func showInvalidInput() {
        self.view?.presentAlert(title: "Notification", message: "The city's name must be at least 3 characters")
    }
    
    public func getWeather(term: String = "Saigon") {
        getWeather(term: term, apiKey: NetworkConstants.openWeatherAPIKey)
    }
    
    public func getWeather(term: String = "Saigon", apiKey: String) {
        if let apiKey = Bundle.main.getInfoByKey(key: apiKey) as? String {
            let params = [
                "q": Sanitier.stringSanitizer(term),
                "cnt": "7",
                "appid": apiKey,
                "units": "metric",
            ]
            self.view?.showLoading()
            weatherService.getWeatherDailyWeather(fromParams: params) { [weak self] result in
                self?.view?.hideLoading()
                switch result {
                case .success(let response):
                    self?.weather = response
                    self?.view?.presentWeather()
                case .failure(let error):
                    self?.weather = nil
                    if error == .noData {
                        self?.view?.presentError(title: "Get weather", message: "Response doesn't have data")
                    } else {
                        self?.view?.presentError(title: "Get weather", message: error.localizedDescription)
                        print(error.localizedDescription)
                    }
                }
                
            }
        } else {
            self.view?.presentError(title: "Error", message: "Something went wrong\rPlease try again")
        }
    }
    
    func weatherForRowIndexPath(indexPath: IndexPath) -> WeatherDetail? {
        if indexPath.row >= 0 &&
            indexPath.row < numbersWeatherInSection {
            return weather?.list?[indexPath.row]
        }
        return nil
    }

    var numbersWeatherInSection: Int {
        return weather?.list?.count ?? 0
    }
    
    func clearData() {
        self.weather = nil
    }
}
