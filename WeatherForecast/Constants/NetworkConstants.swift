//
//  NetworkConstants.swift
//  WeatherForecast
//
//  Created by Bui Dat on 7/20/22.
//  Copyright © 2022 Bui Dat. All rights reserved.
//

import Foundation

enum NetworkConstants {
    static let openWeatherAPIKey = "OPEN_WEATHER_API_KEY"
    static let openWeatherDomain = "api.openweathermap.org"
    static let openWeatherDaily = "/data/2.5/forecast/daily"
    static let openWeatherBaseURL = "\(secureConnection)://\(openWeatherDomain)"
    static let secureConnection = "https"
}
